﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;
using MeetivuTemplate.Business.Education.Interfaces;
using MeetivuTemplate.Models.Request;
using MeetivuTemplate.Models.Response;
using Microsoft.AspNetCore.Mvc;
using Microsoft.Extensions.Configuration;

// For more information on enabling Web API for empty projects, visit https://go.microsoft.com/fwlink/?LinkID=397860

namespace MeetivuTemplate.Controllers
{

    public class EducationController : Controller
    {
        private readonly IConfiguration _configuration;
        private readonly IEducationBusiness _educationBusiness;

        public EducationController(IConfiguration configuration, IEducationBusiness educationBusiness)
        {
            _configuration = configuration;
            _educationBusiness = educationBusiness;
        }


        [HttpPost]
        [Route("api/education/add")]
        public async Task<IActionResult> Add([FromBody] EducationAddRequest request)
        {

            EducationAddResponse response = await _educationBusiness.AddEducationAsync(request);

            return (IActionResult)response;
        }

    }
}
