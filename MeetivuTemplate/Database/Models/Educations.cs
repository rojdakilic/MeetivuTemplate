﻿using System;
namespace MeetivuTemplate.Database.Models
{
    public class Educations
    {
        public int Id { get; set; }
        public string Name { get; set; }
        public string CourseName { get; set; }
    }
}
