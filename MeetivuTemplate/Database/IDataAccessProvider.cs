﻿using System;
using System.Collections.Generic;
using MeetivuTemplate.Database.Models;

namespace MeetivuTemplate.Database
{
    public interface IDataAccessProvider
    {
        void AddEducations(Educations educations);
        void UpdateEducations(Educations educations);
        void DeleteEducations(int id);
        Educations GetEducations(int id);
        List<Educations> GetEducations();
    }
}
