﻿using System;
using System.Collections.Generic;
using System.Linq;
using MeetivuTemplate.Database.Models;

namespace MeetivuTemplate.Database.DataAccess
{
    public class DataAccessProvider: IDataAccessProvider
    {
        private readonly PostgreSqlContext _context;

        public DataAccessProvider(PostgreSqlContext context)
        {
            _context = context;
        }
    
        public void AddEducations(Educations educations)
        {
            _context.educations.Add(educations);
            _context.SaveChanges();
        }

        public void UpdateEducations(Educations educations)
        {
            _context.educations.Update(educations);
            _context.SaveChanges();
        }

        public void DeleteEducations(int id)
        {
            var entity = _context.educations.FirstOrDefault(t => t.Id == id);
            _context.educations.Remove(entity);
            _context.SaveChanges();
        }

        public Educations GetEducations(int id)
        {
            return _context.educations.FirstOrDefault(t => t.Id == id);
        }

        public List<Educations> GetEducations()
        {
            return _context.educations.ToList();
        }

    }
}
