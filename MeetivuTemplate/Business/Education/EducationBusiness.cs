﻿using System;
using System.Threading.Tasks;
using MeetivuTemplate.Business.Education.Interfaces;
using MeetivuTemplate.Models.Request;
using MeetivuTemplate.Models.Response;
using MeetivuTemplate.Repositories.Input;
using MeetivuTemplate.Repositories.Interfaces;
using MeetivuTemplate.Repositories.Output;

namespace MeetivuTemplate.Business.Education
{
    public class EducationBusiness : IEducationBusiness
    {
        private readonly IEducationRepository _educationRepository;

        public EducationBusiness(IEducationRepository educationRepository)
        {
            _educationRepository = educationRepository;
        }

        public async Task<EducationAddResponse> AddEducationAsync(EducationAddRequest request)
        {
            EducationAddInput input = new EducationAddInput
            {
                Name = request.Name,
                CourseName = request.CourseName
            };

            EducationAddOutput output = await _educationRepository.AddEducation(input);

            if (!output.Result)
            {

            }

            EducationAddResponse response = new EducationAddResponse
            {
                Result = output.Result
            };

            return response;
        }
    }
}
