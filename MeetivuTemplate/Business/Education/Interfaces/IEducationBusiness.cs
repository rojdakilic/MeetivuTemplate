﻿using System;
using System.Threading.Tasks;
using MeetivuTemplate.Models.Request;
using MeetivuTemplate.Models.Response;

namespace MeetivuTemplate.Business.Education.Interfaces
{
    public interface IEducationBusiness
    {
        Task<EducationAddResponse> AddEducationAsync(EducationAddRequest request);
    }
}
