﻿using System;
namespace MeetivuTemplate.Repositories.Input
{
    public class EducationAddInput
    {
        public string Name { get; set; }
        public string CourseName { get; set; }

    }

}
