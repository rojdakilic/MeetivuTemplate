﻿using System;
namespace MeetivuTemplate.Repositories.Output
{
    public class EducationAddOutput
    {
        public bool Result { get; set; }
    }
}
