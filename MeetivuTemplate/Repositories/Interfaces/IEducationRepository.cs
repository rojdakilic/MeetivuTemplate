﻿using System;
using System.Threading.Tasks;
using MeetivuTemplate.Repositories.Input;
using MeetivuTemplate.Repositories.Output;

namespace MeetivuTemplate.Repositories.Interfaces
{
    public interface IEducationRepository
    {
        Task<EducationAddOutput> AddEducation(EducationAddInput input);
    }
}
