﻿using System;
using System.Threading.Tasks;
using System.Transactions;
using MeetivuTemplate.Database;
using MeetivuTemplate.Database.Models;
using MeetivuTemplate.Repositories.Input;
using MeetivuTemplate.Repositories.Interfaces;
using MeetivuTemplate.Repositories.Output;

namespace MeetivuTemplate.Repositories
{
    public class EducationRepository : IEducationRepository
    {
        private readonly IDataAccessProvider _dataAccessProvider;

        public EducationRepository(IDataAccessProvider dataAccessProvider)
        {
            _dataAccessProvider = dataAccessProvider;
        }

        public async Task<EducationAddOutput> AddEducation(EducationAddInput input)
        {
            EducationAddOutput output = new EducationAddOutput();

            Educations educations = new Educations
            {
                Id = 1,
                Name = input.Name,
                CourseName = input.CourseName
            };

            using (var scope = new TransactionScope(TransactionScopeAsyncFlowOption.Enabled))
            {
                _dataAccessProvider.AddEducations(educations);
                output.Result = true;

                scope.Complete();

            }

            return output;
            
        }
    }
}