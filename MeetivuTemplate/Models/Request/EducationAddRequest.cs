﻿using System;
namespace MeetivuTemplate.Models.Request
{
    public class EducationAddRequest
    {
        public string Name { get; set; }
        public string CourseName { get; set; }
    }
}
